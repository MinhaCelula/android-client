package xyz.rmunhoz.minhacelula.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import xyz.rmunhoz.minhacelula.R;
import xyz.rmunhoz.minhacelula.ui.activities.settings.SettingsActivity;
import xyz.rmunhoz.minhacelula.ui.fragments.AlterarCelulaFragment;
import xyz.rmunhoz.minhacelula.ui.fragments.BuscarCelulasFragment;
import xyz.rmunhoz.minhacelula.ui.fragments.SuaCelulaFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentManager fragMan;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragMan = getSupportFragmentManager();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        setSupportActionBar(toolbar);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        if (fragMan.findFragmentById(R.id.mainFl) == null) {
            selectNavItem(0);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return toggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    private Boolean selectMenuItem(MenuItem item) {
        int id = item.getItemId();

        Class fragmentClass;

        switch (id) {
            case R.id.nav_sua_celula:
            default:
                fragmentClass = SuaCelulaFragment.class;
                break;
            case R.id.nav_buscar_celulas:
                fragmentClass = BuscarCelulasFragment.class;
                break;
            case R.id.nav_alterar_celula:
                fragmentClass = AlterarCelulaFragment.class;
                break;
            case R.id.nav_config:
                startActivity(new Intent(this, SettingsActivity.class));
                return false;
        }

        replaceFragment(fragmentClass);

        item.setChecked(true);
        setTitle(item.getTitle());
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void selectNavItem(int item) {
        selectMenuItem(navigationView.getMenu().getItem(item));
    }

    private Boolean replaceFragment(Class fragmentClass) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        fragMan.beginTransaction().replace(R.id.mainFl, fragment).commit();

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return selectMenuItem(item);
    }

}
