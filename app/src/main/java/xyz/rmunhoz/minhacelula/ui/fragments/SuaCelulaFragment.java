package xyz.rmunhoz.minhacelula.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import xyz.rmunhoz.minhacelula.R;

public class SuaCelulaFragment extends Fragment {

    public SuaCelulaFragment() {
    }

    public static SuaCelulaFragment newInstance() {
        return new SuaCelulaFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_minha_celula, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnShare = (Button) view.findViewById(R.id.btn_compartilhar_celula);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String horario, endereco, mensagem;

                horario = ((TextView) view.findViewById(R.id.horario_suacelula)).getText().toString();
                endereco = ((TextView) view.findViewById(R.id.endereco_suacelula)).getText().toString();

                mensagem = String.format(getString(R.string.compartilhar_celula_template), horario, endereco);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mensagem);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getString(R.string.compartilhar_com)));
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
