package xyz.rmunhoz.minhacelula.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.rmunhoz.minhacelula.R;

public class AlterarCelulaFragment extends Fragment {

    public AlterarCelulaFragment() {
    }

    public static AlterarCelulaFragment newInstance() {
        return new AlterarCelulaFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_alterar_celula, container, false);
    }

}
