package xyz.rmunhoz.minhacelula.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.rmunhoz.minhacelula.R;

public class BuscarCelulasFragment extends Fragment {

    public BuscarCelulasFragment() {
    }

    public static BuscarCelulasFragment newInstance() {
        return new BuscarCelulasFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_buscar_celulas, container, false);
    }

}
